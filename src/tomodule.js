const pingSettings = {
  image: "assets/sun-ping.png",
  showName: false,
  scale: 2,
  duration: 2,
  rotateSpeed: 1,
  sizeChangeSpeed: 3
};

$(document).ready(() => {
  Hooks.once("ready", async () => {
    const colorMarkers = await (async () => {
      const response = await fetch("modules/tomodule/json/color-markers.json");
      return await response.json();
    })();
    for (const colorMarker of colorMarkers) {
      game.pf2e.ConditionManager.createCustomCondition(colorMarker.name, colorMarker);
    }

    // Set everyone's ping to Tom's preferences
    for (const [name, value] of Object.entries(pingSettings)) {
      game.settings.set("pings", name, value);
    }
  });

  Hooks.on("renderChatMessage", (_message, $message) => {
    // Limit height of non-card chat messages
    $message.find(".message-content:not(:has(.pf2e, .dice-roll))").css({
      "min-height": "2.5em",
      "max-height": "20em",
      "overflow-y": "scroll"
    });

    // e.g., "Frequency: once per round" instead of "Frequency once per round"
    $message.find(".card-content > p > strong").each((_index, subheader) => {
      const $subheader = $(subheader);
      $subheader.text($subheader.text().trim() + ":");

      try {
        const subheaderDesc = $subheader.parents("p").contents().filter(
          (_index, node) => node.nodeType == Node.TEXT_NODE
        )[0];
        subheaderDesc.nodeValue = ` ${subheaderDesc.nodeValue.trim()}`;
      } catch (_) {
        console.log($subheader.parents("p").contents());
      }
    });
  });

  console.log("=== THE TOMODULE SUCCESSFULY ENABLED ===");
});
